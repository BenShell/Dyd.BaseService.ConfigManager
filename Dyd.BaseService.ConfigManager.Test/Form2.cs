﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XXF.Api;
using XXF.BaseService.ConfigManager.Model;

namespace Dyd.BaseService.ConfigManager.Test
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClientResult r = XXF.ProjectTool.ApiHelper.Get("http://localhost:11742/openapi/GetConfig/", new
            {
                token = "dianyadiantesttoken",
                configids = "5"
            });

            if (r.code == 1)
            { 
                var configs = XXF.ProjectTool.ApiHelper.Response(r);
            }

            // public ActionResult SetLoadBalanceBoostPercent(string token, int configid, int sequence, int setboostpercent)
            var o1 = XXF.ProjectTool.ApiHelper.Get("http://localhost:11742/openapi/SetLoadBalanceBoostPercent/", new
            {
                token = "dianyadiantesttoken",//token 网站webconfig中配置token
                configid = 5, //configid
                sequence = 1,
                setboostpercent = 0,
            });
            //public ActionResult SetFailoverSequence(string token, int configid, int failoversequence)
            var o2 = XXF.ProjectTool.ApiHelper.Get("http://localhost:11742/openapi/SetFailoverSequence/", new
            {
                token = "dianyadiantesttoken",//token 网站webconfig中配置token
                configid = 5, //configid
                failoversequence = 1,//
            });
        }
    }

}
