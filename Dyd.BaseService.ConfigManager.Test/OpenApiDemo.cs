﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dyd.BaseService.ConfigManager.Test
{
   public class OpenApiDemo
    {
       public void Demo()
       {
           //获取配置信息接口
           var o = XXF.ProjectTool.ApiHelper.Get("http://localhost:11742/openapi/GetConfig/", new
           {
               token = "dianyadiantesttoken",//token 网站webconfig中配置token
               configids = "5,1" //configids ,分隔多个
           });

           if (o.code == 1)
           {
               var configs = XXF.ProjectTool.ApiHelper.Response(o);
               /*
                这里已经获取到配置信息了
                */
           }

           //负载均衡权重调整接口  public ActionResult SetLoadBalanceBoostPercent(string token, int configid, int sequence, int setboostpercent)
           var o1 = XXF.ProjectTool.ApiHelper.Get("http://localhost:11742/openapi/SetLoadBalanceBoostPercent/", new
           {
               token = "dianyadiantesttoken",//token 网站webconfig中配置token
               configid = 5, //configid
               sequence=1,//负载均衡的顺序号
               setboostpercent=0,//表示将权重设置为0
           });

           //故障转移调整接口 public ActionResult SetFailoverSequence(string token, int configid, int failoversequence)
           var o2 = XXF.ProjectTool.ApiHelper.Get("http://localhost:11742/openapi/SetFailoverSequence/", new
           {
               token = "dianyadiantesttoken",//token 网站webconfig中配置token
               configid = 5, //configid
               failoversequence = 1,//故障转移顺序号：-1 表示关闭故障转移功能 0 表示当前无故障 大于0表示当前配置故障转移列表中的序号
           });
       }
    }
}
