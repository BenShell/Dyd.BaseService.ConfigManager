﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XXF.BaseService.ConfigManager;
using XXF.BaseService.ConfigManager.SystemRuntime;

namespace Dyd.BaseService.ConfigManager.Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }
        private void btnload_Click(object sender, EventArgs e)
        {
            try
            {
                XXF.Common.XXFConfig.ProjectName = this.textBox2.Text.Trim();
                XXF.Common.XXFConfig.ConfigManagerConnectString = this.textBox3.Text.Trim();
                var value = ConfigManagerHelper.Get<string>(this.textBox1.Text);
                this.label1.Text = value;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            //var context = new XXF.BaseService.ConfigManager.SystemRuntime.ConfigInfoOfKeyDic();
            //Dictionary<int, ConfigInfoModel> v = new Dictionary<int, ConfigInfoModel>();
            //v.Add(1, new ConfigInfoModel() { });
            ////context.Add("a", v);
            //Dictionary<int, string> v2 = new Dictionary<int, string>();
            //var json = new XXF.Serialization.JsonHelper().Serializer(v);
        }
    }
}
