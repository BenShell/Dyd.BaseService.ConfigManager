# 统一配置中心 #

用于所有项目统一的配置集中管理，简化运维和项目部署,具备高灵活性，高性能，高稳定性，高及时性。
同时具备配置项的负载均衡和故障转移,从而实现项目的软性的负载均衡和故障转移能力。

## 1.高灵活性 ##
 一个项目可以自由组合或者继承多个分类配置，多个分类的相同配置项可以被子分类配置项覆盖。<br/>
 （举例:项目A可以组合使用“基础数据库配置”，“基础监控配置”，“基础日志配置”，“项目A配置”;项目A配置可以定义自身的特殊配置和覆盖一些基础配置）

## 2.高稳定性 ##
sdk（客户端）默认会在“本地”保存项目的所有配置“快照”，若统一配置中心异常，sdk将从上一次可用“快照”获取配置，并在配置中心恢复后，获取最新版本。

## 3.高性能 ##
sdk（客户端）默认在本地内存中缓存“最新版本”的项目配置，sdk获取的配置都从内存版本中获取。

## 4.高及时性 ##
 
-  sdk（客户端）默认心跳时间内连接配置中心获取最新配置修改。
-  web端的配置修改，通过第三方消息中间件及时通知相关sdk至配置中心获取最新修改。
-  web端可以重启相关分类/相关项目的客户端，重新初始化所有的配置信息。

## 5.配置负载均衡 ##
 可以在web端手工配置某个配置的负载均衡（可以配置不同的使用权重命中不同的负载均衡配置）<br/>
（若要自动，请编写任务挂载到“任务调度”中，根据业务情况，动态调整负载均衡权重。参考openapi接口）

## 6.配置故障转移 ##
 可以在web端手工配置某个配置的故障转移。<br/>
 （若要自动，请编写任务挂载到“任务调度”中，根据业务情况，动态监测当前配置的故障情况，然后选择使用备用故障配置。参考openapi接口）

<br/>
----------

开源相关QQ群: **.net 开源基础服务 238543768**<br/> (大家都有本职工作，也许不能及时响应和跟踪解决问题，请谅解。)

##未来发展##
1. 基于配置的优雅服务/功能降级管理及实现。

<p>
    <strong>使用demo</strong>
</p>
<pre class="brush:c#;toolbar: true; auto-links: false;">using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ConfigManager;//使用ConfigManager命名空间

namespace Dyd.BaseService.ConfigManager.Test
{
    /*
     * 配置中心使用demo
     */
    public class ConfigManagerDemo
    {
        /// &lt;summary&gt;
        /// 配置中心基本配置初始化
        /// &lt;/summary&gt;
        public void InitConfig()
        {
            XXF.Common.XXFConfig.ProjectName = &quot;projecttest&quot;;//当前项目名称,项目代码配置或者从App.config/web.config读取
            XXF.Common.XXFConfig.ConfigManagerConnectString = &quot;server=192.168.17.111;Initial Catalog=dyd_bs_config;User ID=sa;Password=123456;&quot;;//配置中心管理数据库,项目代码配置或者从App.config/web.config读取
        }
        /// &lt;summary&gt;
        /// 使用demo
        /// &lt;/summary&gt;
        public void UseDemo()
        {
            /*
             * 配置获取优先级
             * 1.从本地app.config,web.config中优先获取
             * 2.从配置中心获取次之。
             */
            string configkey = &quot;configkey1&quot;;
            var value = ConfigManagerHelper.Get&lt;string&gt;(configkey);//获取配置值
            
        }
    }
}</pre>

##  部分截图  ##

<p>
    <img src="http://static.oschina.net/uploads/space/2015/1121/142230_pWOa_2379842.jpg"/>
</p>
<p>
    <img src="http://static.oschina.net/uploads/space/2015/1121/142232_Lnov_2379842.jpg"/>
</p>
<p>
    <img src="http://static.oschina.net/uploads/space/2015/1121/142233_tAXa_2379842.jpg"/>
</p>
<p>
    <img src="http://static.oschina.net/uploads/space/2015/1121/142234_XH4w_2379842.jpg"/>
</p>
<p>
    <img src="http://static.oschina.net/uploads/space/2015/1121/142234_BbgN_2379842.jpg"/>
</p>
<p>
    <img src="http://static.oschina.net/uploads/space/2015/1121/142235_cv9I_2379842.jpg"/>
</p>
<p>
    <img src="http://static.oschina.net/uploads/space/2015/1121/142236_crN9_2379842.jpg"/>
</p>
<p>
    <img src="http://static.oschina.net/uploads/space/2015/1121/142237_lFm2_2379842.jpg"/>
</p>

##.net 开源第三方开发学习路线 ##

- 路线1:下载开源源码->学习开源项目->成功部署项目（根据开源文档或者QQ群项目管理员协助）->成为QQ群相关项目管理员->了解并解决日常开源项目问题->总结并整理开源项目文档并分享给大家或推广->成为git项目的开发者和参与者
- 路线2:下载开源源码->学习开源项目->成功部署项目（根据开源文档或者QQ群项目管理员协助）->在实际使用中发现bug并提交bug给项目相关管理员
- 路线3:下载开源源码->学习开源项目->成功部署项目（根据开源文档或者QQ群项目管理员协助）->自行建立开源项目分支->提交分支新功能给项目官方开发人员->官方开发人员根据项目情况合并新功能并发布新版本

## 关于.net 开源生态圈的构想 ##
<b>.net 生态闭环</b>：官方开源项目->第三方参与学习->第三方改进并提交新功能或bug->官方合并新功能或bug->官方发布新版本<br/>
<b>为什么开源?</b> .net 开源生态本身弱,而强大是你与我不断学习，点滴分享,相互协助，共同营造良好的.net生态环境。<br/>
<b>开源理念:</b> 开源是一种态度，分享是一种精神，学习仍需坚持，进步仍需努力，.net生态圈因你我更加美好。<br/>

by 车江毅