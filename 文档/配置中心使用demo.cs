﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ConfigManager;//使用ConfigManager命名空间

namespace Dyd.BaseService.ConfigManager.Test
{
    /*
     * 配置中心使用demo
     */
    public class ConfigManagerDemo
    {
        /// <summary>
        /// 配置中心基本配置初始化
        /// </summary>
        public void InitConfig()
        {
            XXF.Common.XXFConfig.ProjectName = "projecttest";//当前项目名称,项目代码配置或者从App.config/web.config读取
            XXF.Common.XXFConfig.ConfigManagerConnectString = "server=192.168.17.201;Initial Catalog=dyd_bs_config;User ID=sa;Password=Xx~!@#;";//配置中心管理数据库,项目代码配置或者从App.config/web.config读取
        }
        /// <summary>
        /// 使用demo
        /// </summary>
        public void UseDemo()
        {
            /*
             * 配置获取优先级
             * 1.从本地app.config,web.config中优先获取
             * 2.从配置中心获取次之。
             */
            string configkey = "configkey1";
            var value = ConfigManagerHelper.Get<string>(configkey);//获取配置值
            
        }
    }
}
