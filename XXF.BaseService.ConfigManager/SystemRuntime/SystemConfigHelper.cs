﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ConfigManager.Dal;
using XXF.BaseService.ConfigManager.Model;
using XXF.Db;
using XXF.ProjectTool;

namespace XXF.BaseService.ConfigManager.SystemRuntime
{
    public class SystemConfigHelper
    {
        public static string GetRedisServer(DbConn conn)
        {
            try
            {
                List<tb_systemconfig_model> configs = new tb_systemconfig_dal().GetAllList(conn);
                
                foreach (var c in configs)
                {
                    if (c.key.ToLower() == EnumSystemConfigKey.RedisServer.ToString().ToLower())
                    {
                        return c.value;
                    }
                }
                
            }
            catch (Exception exp)
            {
                LogHelper.Error(-1, "加载redisserver配置错误", exp);
            }
            return null;
        }
    }
}
