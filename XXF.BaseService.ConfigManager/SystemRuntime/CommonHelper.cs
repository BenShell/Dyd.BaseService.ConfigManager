﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using XXF.BaseService.ConfigManager.Model;

namespace XXF.BaseService.ConfigManager.SystemRuntime
{
   public  class CommonHelper
    {
       public static List<int> GetProjectCategoryIDs(string categoryids)
       {
           var os  = categoryids.Trim(',').Split(',');
           List<int> rs = new List<int>();
           foreach (var o in os)
           {
               if (!string.IsNullOrWhiteSpace(o))
               {
                   rs.Add(Convert.ToInt32(o));
               }
           }
           rs.Reverse();
           return rs;
       }

       public static List<tb_category_model> OrderByCategoryIDs(List<tb_category_model> models,List<int> categoryids)
       {
           var rs = new List<tb_category_model>();
           foreach (var categoryid in categoryids)
           {
               foreach (var m in models)
               {
                   if (m.id == categoryid)
                   {
                       rs.Add(m);
                   }
               }
           }
           return rs;
       }

       public static Dictionary<string, ConfigInfoModel> OrderByCategoryIDs(Dictionary<string, ConfigInfoModel> models, List<int> categoryids)
       {
           var rs = new Dictionary<string, ConfigInfoModel>();
           foreach (var categoryid in categoryids)
           {
               foreach (var m in models)
               {
                   if (m.Key == categoryid.ToString())
                   {
                       rs.Add(m.Key.ToString(), m.Value);
                   }
               }
           }
           return rs;
       }

       public static List<ConfigInfoModel> OrderByCategoryIDs(List<ConfigInfoModel> models, List<int> categoryids)
       {
           var rs = new List<ConfigInfoModel>();
           foreach (var categoryid in categoryids)
           {
               foreach (var m in models)
               {
                   if (m.configmodel.categoryid == categoryid)
                   {
                       rs.Add(m);
                   }
               }
           }
           return rs;
       }

       public static List<int> GetCategoryIDs(List<tb_category_model> categorymodels)
       {
           List<int> rs = new List<int>();
           foreach (var c in categorymodels)
           {
               rs.Add(c.id);
           }
           return rs;
       }

       /// <summary>
       /// 获取当前服务器默认ip信息
       /// </summary>
       /// <returns></returns>
       public static string GetDefaultIP()
       {
           try
           {
               IPHostEntry ipHost = Dns.Resolve(Dns.GetHostName());
               IPAddress ipAddr = ipHost.AddressList[0];
               return ipAddr.ToString();
           }
           catch (Exception exp)
           { }
           return "";
       }
    }


}
