﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ConfigManager.SystemRuntime
{
    public class ConfigException:Exception
    {
        public ConfigException(string message)
            : base(message)
        { }
    } 
    
    public class DeadLoopConfigException:ConfigException
    {
        public DeadLoopConfigException(string message)
            : base(message)
        { }
    }
}
