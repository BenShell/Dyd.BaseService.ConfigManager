﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ConfigManager.Dal;
using XXF.BaseService.ConfigManager.Model;
using XXF.Extensions;
using XXF.ProjectTool;

namespace XXF.BaseService.ConfigManager.SystemRuntime
{
    public class LogHelper
    {
        public static void Error(int categoryid, string info, Exception exp)
        {
            string errorinfo = string.Format("projectname:{0},categoryid:{1},info:{2},exp:{3}", (AppDomainContext.Context == null ? "" : AppDomainContext.Context.ConfigParams.ProjectName.NullToEmpty()), categoryid, info.NullToEmpty(), exp == null ? "" : exp.Message.NullToEmpty());
            try
            {

                string message = info.NullToEmpty();
                if (exp != null)
                {
                    message += "[错误信息]" + exp.Message;
                }
                System.Diagnostics.Debug.WriteLine(errorinfo);
                SqlHelper.ExcuteSql(AppDomainContext.Context.ConfigParams.ConfigManagerConnectString, false, (c) =>
                {
                    new tb_error_dal().Add(c, new tb_error_model() { categoryid = categoryid, info = message, projectname = AppDomainContext.Context.ConfigParams.ProjectName });
                });
            }
            catch (Exception exp1)
            {
                /*若集成入监控平台,配置获取方面会出现死循环*/
                //XXF.Log.ErrorLog.Write("【统一配置中心】" + errorinfo, exp);
            }
        }

        public static void Log(int categoryid, string info)
        {
            try
            {
                string msg = string.Format("projectname:{0},categoryid:{1},info:{2}", AppDomainContext.Context.ConfigParams.ProjectName.NullToEmpty(), categoryid, info.NullToEmpty());
                System.Diagnostics.Debug.WriteLine(msg);
                SqlHelper.ExcuteSql(AppDomainContext.Context.ConfigParams.ConfigManagerConnectString, false, (c) =>
                {
                    new tb_log_dal().Add(c, new tb_log_model() { categoryid = categoryid, info = info, projectname = AppDomainContext.Context.ConfigParams.ProjectName });
                });
            }
            catch (Exception exp1)
            {
                /*若集成入监控平台,配置获取方面会出现死循环*/
                //XXF.Log.ErrorLog.Write("【统一配置中心】LogHelper-Log错误", exp1);
            }
        }

        public static void Debug(string info)
        {
            System.Diagnostics.Debug.WriteLine(info);
        }
    }
}
