﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ConfigManager.Model;

namespace XXF.BaseService.ConfigManager.SystemRuntime
{
    /// <summary>
    /// 配置信息字典结构
    /// </summary>
    [Serializable]
    public class ConfigInfoOfKeyDic : Dictionary<string, Dictionary<string, ConfigInfoModel >>//关键词,分类,配置信息
    {
        public ConfigInfoModel GetConfig(string configkey, int categoryid)
        {
            if (this.ContainsKey(configkey))
            {
                var item = this[configkey];
                if (item.ContainsKey(categoryid.ToString()))
                {
                    return item[categoryid.ToString()];
                }
            }
            return null;
        }

        public ConfigInfoModel GetConfig(string configkey)
        {
            if (this.ContainsKey(configkey))
            {
                var item = this[configkey];
                if (item.Count>0)
                {
                    return item.Values.ElementAt(0);
                }
            }
            return null;
        }

        public void AddConfig(ConfigInfoModel model)
        {
            if (!this.ContainsKey(model.configmodel.configkey))
            {
                var item = new Dictionary<string, ConfigInfoModel>();
                if (!item.ContainsKey(model.configmodel.categoryid.ToString()))
                    item.Add(model.configmodel.categoryid.ToString(), model);
                this.Add(model.configmodel.configkey, item);
            }
            else
            {
                var item = this[model.configmodel.configkey];
                if (item == null)
                {
                    item = new Dictionary<string, ConfigInfoModel>();
                }
                if (!item.ContainsKey(model.configmodel.categoryid.ToString()))
                    item.Add(model.configmodel.categoryid.ToString(), model);
            }
        }

        public void SetConfig(ConfigInfoModel model,List<int> ordercategoryids)
        {
            if (this.ContainsKey(model.configmodel.configkey))
            {
                var item = this[model.configmodel.configkey];
                if (item.ContainsKey(model.configmodel.categoryid.ToString()))
                {
                    item[model.configmodel.categoryid.ToString()] = model;
                }
                else
                {
                    //复制列表引用，并插入，插入后排序，排序后更新引用
                    var itemnew = new Dictionary<string, ConfigInfoModel>();
                    foreach (var o in item)
                    {
                        itemnew.Add(o.Key,o.Value);
                    }
                    //插入
                    itemnew.Add(model.configmodel.categoryid.ToString(), model);
                    //插入后排序
                    itemnew = CommonHelper.OrderByCategoryIDs(itemnew, ordercategoryids);
                    //排序后更新引用
                    this[model.configmodel.configkey] = itemnew;
                }
            }
            else
            {
                var item = new Dictionary<string, ConfigInfoModel>();
                item.Add(model.configmodel.categoryid.ToString(), model);
                this.Add(model.configmodel.configkey, item);
            }
            
        }

    }
}
