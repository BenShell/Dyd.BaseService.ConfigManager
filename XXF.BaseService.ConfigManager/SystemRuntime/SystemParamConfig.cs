﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ConfigManager.SystemRuntime
{
    public class SystemParamConfig
    {
        /// <summary>
        /// 内部Redis发布订阅通讯通道名
        /// </summary>
        public static string Redis_Channel = "ConfigManager.Redis.Channel";
        /// <summary>
        /// Redis发布订阅通讯通道注册失败间隔重试时间
        /// </summary>
        public static int Redis_Subscribe_FailConnect_ReConnect_Every_Time = 5;
        /// <summary>
        /// sdk心跳到统一配置中心
        /// </summary>
        public static int Config_HeatBeat_Every_Time = 10;
    }
}
