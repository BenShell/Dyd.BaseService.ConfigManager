﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ConfigManager.SystemRuntime
{
    /// <summary>
    /// 配置死循环检测
    /// </summary>
    public class DeadLoopCheck
    {
        public List<string> CallConfigKeyList = new List<string>();
        public int HoldErrorCount = 0;

        public void AddError(string configkey)
        {
            HoldErrorCount++;
            CallConfigKeyList.Add(configkey);

            if (HoldErrorCount > 15)
            {
                string mes = string.Join("->", CallConfigKeyList.ToArray());
                ReSet();
                throw new DeadLoopConfigException("严重错误:配置中心sdk可能出现配置死循环引用，请联系开发人员解决。最后配置链条:" + mes);
            }
            else if (HoldErrorCount > 10)//配置连续出现10次错误
            {
                CallConfigKeyList.Add(configkey);
            }
        }

        public void ReSet()
        {
            HoldErrorCount = 0;
            CallConfigKeyList.Clear();
        }
    }
}
