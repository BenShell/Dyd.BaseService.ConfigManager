using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BaseService.ConfigManager.Model;

namespace XXF.BaseService.ConfigManager.Dal
{
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
	public partial class tb_log_dal
    {

        public virtual bool Add(DbConn PubConn, tb_log_model model)
        {

            List<ProcedureParameter> Par = new List<ProcedureParameter>()
                {
					
					//项目名称
					new ProcedureParameter("@projectname",    model.projectname),
					//分类id
					new ProcedureParameter("@categoryid",    model.categoryid),
					//日志详情
					new ProcedureParameter("@info",    model.info),  
                };
            int rev = PubConn.ExecuteSql(@"insert into tb_log(projectname,categoryid,info,createtime)
										   values(@projectname,@categoryid,@info,getdate())", Par);
            return rev == 1;

        }
    }
}