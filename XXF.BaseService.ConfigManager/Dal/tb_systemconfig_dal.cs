using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BaseService.ConfigManager.Model;
using XXF.ProjectTool;

namespace XXF.BaseService.ConfigManager.Dal
{
	public partial class tb_systemconfig_dal
    {
        public virtual List<tb_systemconfig_model> GetAllList(DbConn PubConn)
        {
            return SqlHelper.Visit((ps) =>
            {
                var rs = new List<tb_systemconfig_model>();
                StringBuilder stringSql = new StringBuilder();
                stringSql.Append(@"select s.* from tb_systemconfig s");
                DataSet ds = new DataSet();
                PubConn.SqlToDataSet(ds, stringSql.ToString(), ps.ToParameters());
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    { 
                        rs.Add(CreateModel(dr));
                    }
                }
                return rs;
            });
        }

        public virtual tb_systemconfig_model Get(DbConn PubConn, string key)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@key", key));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_systemconfig s where s.key=@key");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
				return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

		public virtual tb_systemconfig_model CreateModel(DataRow dr)
        {
            var o = new tb_systemconfig_model();
			
			//key
			if(dr.Table.Columns.Contains("key"))
			{
				o.key = dr["key"].Tostring();
			}
			//ֵ
			if(dr.Table.Columns.Contains("value"))
			{
				o.value = dr["value"].Tostring();
			}
			//��ע
			if(dr.Table.Columns.Contains("remark"))
			{
				o.remark = dr["remark"].Tostring();
			}
			return o;
        }
    }
}