using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BaseService.ConfigManager.Model;
using XXF.ProjectTool;

namespace XXF.BaseService.ConfigManager.Dal
{
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
	public partial class tb_failover_dal
    {
        public virtual List<tb_failover_model> GetListByCategoryIDs(DbConn PubConn, List<int> configids)
        {
            return SqlHelper.Visit((ps) =>
            {
                var rs = new List<tb_failover_model>();
                if (configids != null && configids.Count > 0)
                {
                    StringBuilder stringSql = new StringBuilder();
                    stringSql.AppendFormat(@"select s.* from tb_failover s where s.configid in ({0})", SqlHelper.CmdInFromListForSimplePar<int>(ps, configids, "configid"));
                    DataSet ds = new DataSet();
                    PubConn.SqlToDataSet(ds, stringSql.ToString(), ps.ToParameters());
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            rs.Add(CreateModel(dr));
                        }
                    }
                }
                return rs;
            });
        }

        public virtual tb_failover_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_failover s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
				return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

		public virtual tb_failover_model CreateModel(DataRow dr)
        {
            var o = new tb_failover_model();
			
			//
			if(dr.Table.Columns.Contains("id"))
			{
				o.id = dr["id"].Toint();
			}
			//故障转移配置id
			if(dr.Table.Columns.Contains("configid"))
			{
				o.configid = dr["configid"].Toint();
			}
			//故障转移值
			if(dr.Table.Columns.Contains("value"))
			{
				o.value = dr["value"].Tostring();
			}
			//故障转移顺序号
			if(dr.Table.Columns.Contains("sequence"))
			{
				o.sequence = dr["sequence"].Toshort();
			}
			return o;
        }
    }
}