using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.ProjectTool;
using XXF.BaseService.ConfigManager.Model;

namespace XXF.BaseService.ConfigManager.Dal
{
	public partial class tb_project_dal
    {
        public virtual tb_project_model Get(DbConn PubConn, string projectname)
        {
            return SqlHelper.Visit((ps) =>
            {
                ps.Add("projectname", projectname);
                StringBuilder stringSql = new StringBuilder();
                stringSql.Append(@"select s.* from tb_project s where s.projectname=@projectname");
                DataSet ds = new DataSet();
                PubConn.SqlToDataSet(ds, stringSql.ToString(), ps.ToParameters());
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return CreateModel(ds.Tables[0].Rows[0]);
                }
                return null;
            });
        }

        public virtual void UpdateLastheartbeattime(DbConn PubConn, int id)
        {
            SqlHelper.Visit((ps) =>
            {
                ps.Add("id", id);
                StringBuilder stringSql = new StringBuilder();
                stringSql.Append(@"update tb_project set lastheartbeattime=getdate() where id=@id");
                return PubConn.ExecuteSql(stringSql.ToString(), ps.ToParameters());
            });
        }

        public virtual tb_project_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_project s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
				return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public virtual tb_project_model CreateModel(DataRow dr)
        {
            var o = new tb_project_model();

            //
            if (dr.Table.Columns.Contains("id"))
            {
                o.id = dr["id"].Toint();
            }
            //项目名称
            if (dr.Table.Columns.Contains("projectname"))
            {
                o.projectname = dr["projectname"].Tostring();
            }
            //项目备注
            if (dr.Table.Columns.Contains("remark"))
            {
                o.remark = dr["remark"].Tostring();
            }
            //创建时间
            if (dr.Table.Columns.Contains("createtime"))
            {
                o.createtime = dr["createtime"].ToDateTime();
            }
            //相关分类id，号分隔，号结尾
            if (dr.Table.Columns.Contains("categoryids"))
            {
                o.categoryids = dr["categoryids"].Tostring();
            }
            //项目最后心跳时间
            if (dr.Table.Columns.Contains("lastheartbeattime"))
            {
                o.lastheartbeattime = dr["lastheartbeattime"].ToDateTime();
            }
            return o;
        }
    }
}