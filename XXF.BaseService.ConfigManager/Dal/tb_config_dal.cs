using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BaseService.ConfigManager.Model;
using XXF.ProjectTool;

namespace XXF.BaseService.ConfigManager.Dal
{
    public partial class tb_config_dal
    {
        public virtual List<tb_config_model> GetListByCategoryIDs(DbConn PubConn, List<int> categoryids, DateTime lastupdatetime)
        {
            return SqlHelper.Visit((ps) =>
            {
                var rs = new List<tb_config_model>();
                if (categoryids != null && categoryids.Count > 0)
                {
                    StringBuilder stringSql = new StringBuilder();
                    stringSql.AppendFormat(@"select s.* from tb_config s with (nolock) where s.categoryid in ({0}) and [lastupdatetime]>@lastupdatetime", SqlHelper.CmdInFromListForSimplePar<int>(ps, categoryids, "categoryid"));
                    ps.Add("lastupdatetime", lastupdatetime);
                    DataSet ds = new DataSet();
                    PubConn.SqlToDataSet(ds, stringSql.ToString(), ps.ToParameters());
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            rs.Add(CreateModel(dr));
                        }
                    }
                }
                return rs;
            });
        }

        public virtual tb_config_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_config s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public virtual tb_config_model CreateModel(DataRow dr)
        {
            var o = new tb_config_model();

            //
            if (dr.Table.Columns.Contains("id"))
            {
                o.id = dr["id"].Toint();
            }
            //分类id
            if (dr.Table.Columns.Contains("categoryid"))
            {
                o.categoryid = dr["categoryid"].Toint();
            }
            //配置Key
            if (dr.Table.Columns.Contains("configkey"))
            {
                o.configkey = dr["configkey"].Tostring();
            }
            //配置值
            if (dr.Table.Columns.Contains("configvalue"))
            {
                o.configvalue = dr["configvalue"].Tostring();
            }
            //配置备注
            if (dr.Table.Columns.Contains("remark"))
            {
                o.remark = dr["remark"].Tostring();
            }
            //上一次修改时间
            if (dr.Table.Columns.Contains("lastupdatetime"))
            {
                o.lastupdatetime = dr["lastupdatetime"].ToDateTime();
            }
            //创建时间
            if (dr.Table.Columns.Contains("createtime"))
            {
                o.createtime = dr["createtime"].ToDateTime();
            }
            //负载均衡算法类型: -1 表示 关闭负载均衡，1 表示随机负载均衡算法，2表示顺序负载均衡算法
            if (dr.Table.Columns.Contains("loadbalancealgorithmenum"))
            {
                o.loadbalancealgorithmenum = dr["loadbalancealgorithmenum"].Toshort();
            }
            //故障转移顺序号：-1 表示关闭故障转移 0 表示当前无故障 大于0表示当前配置故障转移列表中的序号
            if (dr.Table.Columns.Contains("failoversequence"))
            {
                o.failoversequence = dr["failoversequence"].Toshort();
            }
            return o;
        }
    }
}