using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BaseService.ConfigManager.Model
{
    /// <summary>
    /// tb_config Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_config_model
    {
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
        
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
        
        /// <summary>
        /// 分类id
        /// </summary>
        public int categoryid { get; set; }
        
        /// <summary>
        /// 配置Key
        /// </summary>
        public string configkey { get; set; }
        
        /// <summary>
        /// 配置值
        /// </summary>
        public string configvalue { get; set; }
        
        /// <summary>
        /// 配置备注
        /// </summary>
        public string remark { get; set; }
        
        /// <summary>
        /// 上一次修改时间
        /// </summary>
        public DateTime lastupdatetime { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime createtime { get; set; }
        
        /// <summary>
        /// 负载均衡算法类型: -1 表示 关闭负载均衡，1 表示随机负载均衡算法，2表示顺序负载均衡算法
        /// </summary>
        public short loadbalancealgorithmenum { get; set; }
        
        /// <summary>
        /// 故障转移顺序号：-1 表示关闭故障转移 0 表示当前无故障 大于0表示当前配置故障转移列表中的序号
        /// </summary>
        public short failoversequence { get; set; }
        
    }
}