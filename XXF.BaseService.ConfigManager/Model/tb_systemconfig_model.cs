using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BaseService.ConfigManager.Model
{
    /// <summary>
    /// tb_systemconfig Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_systemconfig_model
    {
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
        
        /// <summary>
        /// key
        /// </summary>
        public string key { get; set; }
        
        /// <summary>
        /// 值
        /// </summary>
        public string value { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }
        
    }
}