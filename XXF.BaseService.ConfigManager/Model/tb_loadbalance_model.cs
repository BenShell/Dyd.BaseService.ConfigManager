using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BaseService.ConfigManager.Model
{
    /// <summary>
    /// tb_loadbalance Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_loadbalance_model
    {
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 负载均衡配置id
        /// </summary>
        public int configid { get; set; }

        /// <summary>
        /// 负载均衡值
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// 负载均衡权重(1-100之间的int)，负载均衡总值不超过100
        /// </summary>
        public int boostpercent { get; set; }

        /// <summary>
        /// 负载均衡序号
        /// </summary>
        public short sequence { get; set; }
    }
}