using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BaseService.ConfigManager.Model
{
    /// <summary>
    /// tb_failover Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_failover_model
    {
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
        
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
        
        /// <summary>
        /// 故障转移配置id
        /// </summary>
        public int configid { get; set; }
        
        /// <summary>
        /// 故障转移值
        /// </summary>
        public string value { get; set; }
        
        /// <summary>
        /// 故障转移顺序号
        /// </summary>
        public short sequence { get; set; }
        
    }
}