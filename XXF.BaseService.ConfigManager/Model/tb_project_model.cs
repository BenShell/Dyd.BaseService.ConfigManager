using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BaseService.ConfigManager.Model
{
    /// <summary>
    /// tb_project Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_project_model
    {
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
        
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
        
        /// <summary>
        /// 项目名称
        /// </summary>
        public string projectname { get; set; }
        
        /// <summary>
        /// 项目备注
        /// </summary>
        public string remark { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime createtime { get; set; }
        
        /// <summary>
        /// 相关分类id，号分隔，号结尾
        /// </summary>
        public string categoryids { get; set; }
        
        /// <summary>
        /// 项目最后心跳时间
        /// </summary>
        public DateTime lastheartbeattime { get; set; }
        
    }
}