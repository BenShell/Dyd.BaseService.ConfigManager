using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using Dyd.BaseService.ConfigManager.Domain.Model;

namespace Dyd.BaseService.ConfigManager.Domain.Dal
{
    public partial class tb_category_dal
    {
        #region C
        public virtual int Add(DbConn PubConn, tb_category_model model)
        {

            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //分类名
            Par.Add(new ProcedureParameter("@name", model.name));
            //分类备注
            Par.Add(new ProcedureParameter("@remark", model.remark));
            //创建时间
            Par.Add(new ProcedureParameter("@createtime", model.createtime));
            object rev = PubConn.ExecuteScalar(@"insert into tb_category(name,remark,createtime)
                                           values(@name,@remark,@createtime);select @@IDENTITY", Par);
            var id = Convert.ToInt32(rev);
            return id;

        }

        public virtual bool Edit(DbConn PubConn, tb_category_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //分类名
            Par.Add(new ProcedureParameter("@name", model.name));
            //分类备注
            Par.Add(new ProcedureParameter("@remark", model.remark));
            //创建时间
            Par.Add(new ProcedureParameter("@createtime", model.createtime));

            Par.Add(new ProcedureParameter("@id", model.id));

            int rev = PubConn.ExecuteSql("update tb_category set name=@name,remark=@remark,createtime=@createtime where id=@id", Par);
            return rev == 1;
        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));

            string Sql = "delete from tb_category where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        #endregion

        #region Q

        public bool IsExist(DbConn conn, tb_category_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("name", model.name));
            Par.Add(new ProcedureParameter("id", model.id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select top 1 id from tb_category s where name=@name and id<>@id");
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public OperateResults DelCheck(DbConn conn, int id)
        {
            OperateResults re = new OperateResults { Flag = true };
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("categoryid", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select top 1 id from tb_config s where categoryid=@categoryid");
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                re.Flag = false;
                re.Message += "分类下存在配置，不能删除！";
            }

            List<ProcedureParameter> Par2 = new List<ProcedureParameter>();
            Par2.Add(new ProcedureParameter("categoryid", id.ToString()));

            stringSql.Clear();
            stringSql.Append(@"select top 1 id from tb_project s where categoryids like '%,'+@categoryid+',%'");
            conn.SqlToDataSet(ds, stringSql.ToString(), Par2);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                re.Flag = false;
                re.Message += "项目引用当前配置，不能删除！";
            }
            return re;
        }

        public virtual tb_category_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_category s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public IList<tb_category_model> GetPageList(DbConn conn, string name, string remark, int pageSize, int pageIndex, ref int count)
        {
            int tempCount = 0;

            IList<tb_category_model> list = new List<tb_category_model>();
            StringBuilder where = new StringBuilder();
            List<ProcedureParameter> parameters = new List<ProcedureParameter>();
            where.Append(" where 1=1");
            if (!string.IsNullOrWhiteSpace(name))
            {
                parameters.Add(new ProcedureParameter("name", name));
                where.Append(" AND name like '%'+@name+'%' ");
            }
            if (!string.IsNullOrWhiteSpace(remark))
            {
                parameters.Add(new ProcedureParameter("remark", remark));
                where.Append(" AND remark like '%'+@remark+'%' ");
            }
            StringBuilder sql = new StringBuilder();
            sql.Append("select row_number() over(order by id desc) as rownum,* from tb_category with(nolock)");
            string countSql = string.Concat("select count(1) from tb_category with(nolock) ", where.ToString());
            object obj = conn.ExecuteScalar(countSql, parameters);
            if (obj != DBNull.Value && obj != null)
            {
                tempCount = LibConvert.ObjToInt(obj);
            }
            string sqlPage = string.Concat("select * from (", sql.ToString(), where.ToString(), ") a where rownum between ", ((pageIndex - 1) * pageSize + 1), " and ", pageSize * pageIndex);
            DataTable dt = conn.SqlToDataTable(sqlPage, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    tb_category_model model = CreateModel(dr);
                    list.Add(model);
                }
            }
            count = tempCount;
            return list;

        }

        public IList<tb_category_model> GetList(DbConn conn)
        {
            List<ProcedureParameter> parameters = new List<ProcedureParameter>();
            IList<tb_category_model> list = new List<tb_category_model>();

            StringBuilder sql = new StringBuilder();
            sql.Append("select * from tb_category with(nolock) order by id desc");

            DataTable dt = conn.SqlToDataTable(sql.ToString(), parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    tb_category_model model = CreateModel(dr);
                    list.Add(model);
                }
            }
            return list;
        }


        public IList<tb_category_model> GetListByIds(DbConn conn, string Ids)
        {
            IList<tb_category_model> list = new List<tb_category_model>();
            StringBuilder where = new StringBuilder();
            List<ProcedureParameter> parameters = new List<ProcedureParameter>();

            StringBuilder sql = new StringBuilder();
            sql.Append("select row_number() over(order by id desc) as rownum,* from tb_category with(nolock) ");
            sql.Append(" where 1=1");
            if (!string.IsNullOrWhiteSpace(Ids))
            {
                sql.Append(" and id in (" + Ids + ")");
            }
            DataTable dt = conn.SqlToDataTable(sql.ToString(), parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    tb_category_model model = CreateModel(dr);
                    list.Add(model);
                }
            }
            return list;
        }

        #endregion

        public virtual tb_category_model CreateModel(DataRow dr)
        {
            var o = new tb_category_model();

            //
            if (dr.Table.Columns.Contains("id"))
            {
                o.id = dr["id"].Toint();
            }
            //分类名
            if (dr.Table.Columns.Contains("name"))
            {
                o.name = dr["name"].Tostring();
            }
            //分类备注
            if (dr.Table.Columns.Contains("remark"))
            {
                o.remark = dr["remark"].Tostring();
            }
            //创建时间
            if (dr.Table.Columns.Contains("createtime"))
            {
                o.createtime = dr["createtime"].ToDateTime();
            }
            return o;
        }

    }
}