using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using Dyd.BaseService.ConfigManager.Domain.Model;

namespace Dyd.BaseService.ConfigManager.Domain.Dal
{

    public partial class tb_log_dal
    {
        #region C
        public virtual bool Add(DbConn PubConn, tb_log_model model)
        {

            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //项目名称
            Par.Add(new ProcedureParameter("@projectname", model.projectname));
            //分类id
            Par.Add(new ProcedureParameter("@categoryid", model.categoryid));
            //日志详情
            Par.Add(new ProcedureParameter("@info", model.info));
            //创建时间
            Par.Add(new ProcedureParameter("@createtime", model.createtime));
            int rev = PubConn.ExecuteSql(@"insert into tb_log(projectname,categoryid,info,createtime)
                                           values(@projectname,@categoryid,@info,@createtime)", Par);
            return rev == 1;

        }

        public int Add(DbConn PubConn, string info, string projectname = "", int categoryid = 0)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //项目名称
            Par.Add(new ProcedureParameter("@projectname", projectname));
            //分类id
            Par.Add(new ProcedureParameter("@categoryid", categoryid));
            //日志详情
            Par.Add(new ProcedureParameter("@info", info));
            //创建时间
            Par.Add(new ProcedureParameter("@createtime", PubConn.GetServerDate()));
            object rev = PubConn.ExecuteScalar(@"insert into tb_log(projectname,categoryid,info,createtime)
                                           values(@projectname,@categoryid,@info,@createtime);select @@IDENTITY", Par);
            var id = Convert.ToInt32(rev);
            return id;
        }

        public virtual bool Edit(DbConn PubConn, tb_log_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //项目名称
            Par.Add(new ProcedureParameter("@projectname", model.projectname));
            //分类id
            Par.Add(new ProcedureParameter("@categoryid", model.categoryid));
            //日志详情
            Par.Add(new ProcedureParameter("@info", model.info));
            //创建时间
            Par.Add(new ProcedureParameter("@createtime", model.createtime));

            Par.Add(new ProcedureParameter("@id", model.id));

            int rev = PubConn.ExecuteSql("update tb_log set projectname=@projectname,categoryid=@categoryid,info=@info,createtime=@createtime where id=@id", Par);
            return rev == 1;

        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));

            string Sql = "delete from tb_log where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual bool Clear(DbConn PubConn)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            string Sql = "truncate table tb_log ";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Q

        public virtual tb_log_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_log s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public IList<tb_log_model> GetPageList(DbConn conn, DateTime? startTime, DateTime? endTime, string projectname, string info, int pageSize, int pageIndex, ref int count)
        {
            int tempCount = 0;

            IList<tb_log_model> list = new List<tb_log_model>();
            StringBuilder where = new StringBuilder();
            List<ProcedureParameter> parameters = new List<ProcedureParameter>();
            where.Append(" where 1=1");
            if (startTime != null && endTime != null)
            {
                parameters.Add(new ProcedureParameter("startTime", startTime.Value.ToString("yyyy-MM-dd")));
                parameters.Add(new ProcedureParameter("endTime", endTime.Value.ToString("yyyy-MM-dd")));
                where.Append(" and m.createtime>=@starttime and m.createtime<=@endTime ");
            }
            if (!string.IsNullOrWhiteSpace(projectname))
            {
                parameters.Add(new ProcedureParameter("projectname", projectname));
                where.Append(" AND m.projectname like '%'+@projectname+'%' ");
            }
            if (!string.IsNullOrWhiteSpace(info))
            {
                parameters.Add(new ProcedureParameter("info", info));
                where.Append(" AND m.info like '%'+@info+'%' ");
            }
            StringBuilder sql = new StringBuilder();
            sql.Append("select row_number() over(order by m.id desc) as rownum,m.*,s.name as categoryname from tb_log as m with(nolock) left join tb_category as s with(nolock) on m.categoryid=s.id");
            string countSql = string.Concat("select count(1) from tb_log as m with(nolock) ", where.ToString());
            object obj = conn.ExecuteScalar(countSql, parameters);
            if (obj != DBNull.Value && obj != null)
            {
                tempCount = LibConvert.ObjToInt(obj);
            }
            string sqlPage = string.Concat("select * from (", sql.ToString(), where.ToString(), ") a where rownum between ", ((pageIndex - 1) * pageSize + 1), " and ", pageSize * pageIndex);
            DataTable dt = conn.SqlToDataTable(sqlPage, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    tb_log_model model = CreateModel(dr);
                    list.Add(model);
                }
            }
            count = tempCount;
            return list;

        }

        #endregion

        #region helper

        public virtual tb_log_model CreateModel(DataRow dr)
        {
            var o = new tb_log_model();

            //
            if (dr.Table.Columns.Contains("id"))
            {
                o.id = dr["id"].Toint();
            }
            //项目名称
            if (dr.Table.Columns.Contains("projectname"))
            {
                o.projectname = dr["projectname"].Tostring();
            }
            //分类id
            if (dr.Table.Columns.Contains("categoryid"))
            {
                o.categoryid = dr["categoryid"].Toint();
            }
            //日志详情
            if (dr.Table.Columns.Contains("info"))
            {
                o.info = dr["info"].Tostring();
            }
            //创建时间
            if (dr.Table.Columns.Contains("createtime"))
            {
                o.createtime = dr["createtime"].ToDateTime();
            }

            //分类名称
            if (dr.Table.Columns.Contains("categoryname"))
            {
                o.categoryname = Convert.ToString(dr["categoryname"]);
            }
            return o;
        }

        #endregion
    }
}