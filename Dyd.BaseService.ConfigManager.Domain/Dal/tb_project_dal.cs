using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using Dyd.BaseService.ConfigManager.Domain.Model;

namespace Dyd.BaseService.ConfigManager.Domain.Dal
{
    /*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
    public partial class tb_project_dal
    {
        #region C
        public virtual int Add(DbConn PubConn, tb_project_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //项目名称
            Par.Add(new ProcedureParameter("@projectname", model.projectname));
            //项目备注
            Par.Add(new ProcedureParameter("@remark", model.remark));
            //创建时间
            Par.Add(new ProcedureParameter("@createtime", model.createtime));
            //相关分类id，号分隔，号结尾
            Par.Add(new ProcedureParameter("@categoryids", model.categoryids));
            //项目最后心跳时间
            Par.Add(new ProcedureParameter("@lastheartbeattime", model.lastheartbeattime));
            object rev = PubConn.ExecuteScalar(@"insert into tb_project(projectname,remark,createtime,categoryids,lastheartbeattime)
                                           values(@projectname,@remark,@createtime,@categoryids,@lastheartbeattime);select @@IDENTITY", Par);
            var id = Convert.ToInt32(rev);
            return id;
        }

        public virtual bool Edit(DbConn PubConn, tb_project_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //项目名称
            Par.Add(new ProcedureParameter("@projectname", model.projectname));
            //项目备注
            Par.Add(new ProcedureParameter("@remark", model.remark));
            //创建时间
            Par.Add(new ProcedureParameter("@createtime", model.createtime));
            //相关分类id，号分隔，号结尾
            Par.Add(new ProcedureParameter("@categoryids", model.categoryids));
            //项目最后心跳时间
            Par.Add(new ProcedureParameter("@lastheartbeattime", model.lastheartbeattime));

            Par.Add(new ProcedureParameter("@id", model.id));

            int rev = PubConn.ExecuteSql("update tb_project set projectname=@projectname,remark=@remark,createtime=@createtime,categoryids=@categoryids,lastheartbeattime=@lastheartbeattime where id=@id", Par);
            return rev == 1;

        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));

            string Sql = "delete from tb_project where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        #endregion

        #region Q
        public virtual tb_project_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_project s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public IList<tb_project_model> GetPageList(DbConn conn, int categoryid, string projectname, string remark, int pageSize, int pageIndex, ref int count)
        {
            int tempCount = 0;

            IList<tb_project_model> list = new List<tb_project_model>();
            StringBuilder where = new StringBuilder();
            List<ProcedureParameter> parameters = new List<ProcedureParameter>();
            where.Append(" where 1=1");
            if (categoryid > 0)
            {
                parameters.Add(new ProcedureParameter("categoryid", categoryid.ToString()));
                where.Append(" AND categoryids like '%,'+@categoryid+',%' ");
            }
            if (!string.IsNullOrWhiteSpace(projectname))
            {
                parameters.Add(new ProcedureParameter("projectname", projectname));
                where.Append(" AND projectname like '%'+@projectname+'%' ");
            }
            if (!string.IsNullOrWhiteSpace(remark))
            {
                parameters.Add(new ProcedureParameter("remark", remark));
                where.Append(" AND [remark] like '%'+@remark+'%' ");
            }
            StringBuilder sql = new StringBuilder();
            sql.Append("select row_number() over(order by id desc) as rownum,* from tb_project with(nolock)");
            string countSql = string.Concat("select count(1) from tb_project with(nolock) ", where.ToString());
            object obj = conn.ExecuteScalar(countSql, parameters);
            if (obj != DBNull.Value && obj != null)
            {
                tempCount = LibConvert.ObjToInt(obj);
            }
            string sqlPage = string.Concat("select * from (", sql.ToString(), where.ToString(), ") a where rownum between ", ((pageIndex - 1) * pageSize + 1), " and ", pageSize * pageIndex);
            DataTable dt = conn.SqlToDataTable(sqlPage, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    tb_project_model model = CreateModel(dr);
                    list.Add(model);
                }
            }
            count = tempCount;
            return list;

        }
        #endregion

        public virtual tb_project_model CreateModel(DataRow dr)
        {
            var o = new tb_project_model();

            //
            if (dr.Table.Columns.Contains("id"))
            {
                o.id = dr["id"].Toint();
            }
            //项目名称
            if (dr.Table.Columns.Contains("projectname"))
            {
                o.projectname = dr["projectname"].Tostring();
            }
            //项目备注
            if (dr.Table.Columns.Contains("remark"))
            {
                o.remark = dr["remark"].Tostring();
            }
            //创建时间
            if (dr.Table.Columns.Contains("createtime"))
            {
                o.createtime = dr["createtime"].ToDateTime();
            }
            //相关分类id，号分隔，号结尾
            if (dr.Table.Columns.Contains("categoryids"))
            {
                o.categoryids = dr["categoryids"].Tostring();
            }
            //项目最后心跳时间
            if (dr.Table.Columns.Contains("lastheartbeattime"))
            {
                o.lastheartbeattime = dr["lastheartbeattime"].ToDateTime();
            }
            return o;
        }
    }
}