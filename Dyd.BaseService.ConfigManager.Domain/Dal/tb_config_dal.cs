using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using Dyd.BaseService.ConfigManager.Domain.Model;

namespace Dyd.BaseService.ConfigManager.Domain.Dal
{
    public partial class tb_config_dal
    {
        #region C
        public virtual bool Add(DbConn PubConn, tb_config_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //分类id
            Par.Add(new ProcedureParameter("@categoryid", model.categoryid));
            //配置Key
            Par.Add(new ProcedureParameter("@configkey", model.configkey));
            //配置值
            Par.Add(new ProcedureParameter("@configvalue", model.configvalue));
            //配置备注
            Par.Add(new ProcedureParameter("@remark", model.remark));
            //上一次修改时间
            Par.Add(new ProcedureParameter("@lastupdatetime", model.lastupdatetime));
            //创建时间
            Par.Add(new ProcedureParameter("@createtime", model.createtime));
            //负载均衡算法类型: -1 表示 关闭负载均衡，1 表示负载均衡算法
            Par.Add(new ProcedureParameter("@loadbalancealgorithmenum", model.loadbalancealgorithmenum));
            //故障转移顺序号：-1 表示关闭故障转移 0 表示当前无故障 大于0表示当前配置故障转移列表中的序号
            Par.Add(new ProcedureParameter("@failoversequence", model.failoversequence));
            int rev = PubConn.ExecuteSql(@"insert into tb_config(categoryid,configkey,configvalue,remark,lastupdatetime,createtime,loadbalancealgorithmenum,failoversequence)
                                           values(@categoryid,@configkey,@configvalue,@remark,@lastupdatetime,@createtime,@loadbalancealgorithmenum,@failoversequence)", Par);
            return rev == 1;

        }

        public virtual int Add(DbConn PubConn, tb_config_model model, List<tb_failover_model> failoverList, List<tb_loadbalance_model> loadBalanceList)
        {
            tb_failover_dal failoverDal = new tb_failover_dal();
            tb_loadbalance_dal loadbalanceDal = new tb_loadbalance_dal();

            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //分类id
            Par.Add(new ProcedureParameter("@categoryid", model.categoryid));
            //配置Key
            Par.Add(new ProcedureParameter("@configkey", model.configkey));
            //配置值
            Par.Add(new ProcedureParameter("@configvalue", model.configvalue));
            //配置备注
            Par.Add(new ProcedureParameter("@remark", model.remark));
            //上一次修改时间
            Par.Add(new ProcedureParameter("@lastupdatetime", model.lastupdatetime));
            //创建时间
            Par.Add(new ProcedureParameter("@createtime", model.createtime));
            //负载均衡算法类型: -1 表示 关闭负载均衡，1 表示负载均衡算法
            Par.Add(new ProcedureParameter("@loadbalancealgorithmenum", model.loadbalancealgorithmenum));
            //故障转移顺序号：-1 表示关闭故障转移 0 表示当前无故障 大于0表示当前配置故障转移列表中的序号
            Par.Add(new ProcedureParameter("@failoversequence", model.failoversequence));
            object rev = PubConn.ExecuteScalar(@"insert into tb_config(categoryid,configkey,configvalue,remark,lastupdatetime,createtime,loadbalancealgorithmenum,failoversequence)
                                           values(@categoryid,@configkey,@configvalue,@remark,@lastupdatetime,@createtime,@loadbalancealgorithmenum,@failoversequence);select @@IDENTITY", Par);
            var configId = Convert.ToInt32(rev);
            string sql = string.Empty;
            foreach (var item in failoverList)
            {
                item.configid = configId;
                failoverDal.Add(PubConn, item);
            }
            foreach (var item in loadBalanceList)
            {
                item.configid = configId;
                loadbalanceDal.Add(PubConn, item);
            }
            return configId;
        }

        public virtual bool Edit(DbConn PubConn, tb_config_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //分类id
            Par.Add(new ProcedureParameter("@categoryid", model.categoryid));
            //配置Key
            Par.Add(new ProcedureParameter("@configkey", model.configkey));
            //配置值
            Par.Add(new ProcedureParameter("@configvalue", model.configvalue));
            //配置备注
            Par.Add(new ProcedureParameter("@remark", model.remark));
            //上一次修改时间
            Par.Add(new ProcedureParameter("@lastupdatetime", model.lastupdatetime));
            //创建时间
            Par.Add(new ProcedureParameter("@createtime", model.createtime));
            //负载均衡算法类型: -1 表示 关闭负载均衡，1 表示负载均衡算法
            Par.Add(new ProcedureParameter("@loadbalancealgorithmenum", model.loadbalancealgorithmenum));
            //故障转移顺序号：-1 表示关闭故障转移 0 表示当前无故障 大于0表示当前配置故障转移列表中的序号
            Par.Add(new ProcedureParameter("@failoversequence", model.failoversequence));

            Par.Add(new ProcedureParameter("@id", model.id));

            int rev = PubConn.ExecuteSql("update tb_config set categoryid=@categoryid,configkey=@configkey,configvalue=@configvalue,remark=@remark,lastupdatetime=@lastupdatetime,createtime=@createtime,loadbalancealgorithmenum=@loadbalancealgorithmenum,failoversequence=@failoversequence where id=@id", Par);
            return rev == 1;

        }

        public virtual bool Edit(DbConn PubConn, tb_config_model model, List<tb_failover_model> failoverList, List<tb_loadbalance_model> loadBalanceList)
        {
            tb_failover_dal failoverDal = new tb_failover_dal();
            tb_loadbalance_dal loadbalanceDal = new tb_loadbalance_dal();

            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //分类id
            Par.Add(new ProcedureParameter("@categoryid", model.categoryid));
            //配置Key
            Par.Add(new ProcedureParameter("@configkey", model.configkey));
            //配置值
            Par.Add(new ProcedureParameter("@configvalue", model.configvalue));
            //配置备注
            Par.Add(new ProcedureParameter("@remark", model.remark));
            //上一次修改时间
            Par.Add(new ProcedureParameter("@lastupdatetime", model.lastupdatetime));
            //创建时间
            Par.Add(new ProcedureParameter("@createtime", model.createtime));
            //负载均衡算法类型: -1 表示 关闭负载均衡，1 表示负载均衡算法
            Par.Add(new ProcedureParameter("@loadbalancealgorithmenum", model.loadbalancealgorithmenum));
            //故障转移顺序号：-1 表示关闭故障转移 0 表示当前无故障 大于0表示当前配置故障转移列表中的序号
            Par.Add(new ProcedureParameter("@failoversequence", model.failoversequence));

            Par.Add(new ProcedureParameter("@id", model.id));

            int rev = PubConn.ExecuteSql("update tb_config set categoryid=@categoryid,configkey=@configkey,configvalue=@configvalue,remark=@remark,lastupdatetime=@lastupdatetime,createtime=@createtime,loadbalancealgorithmenum=@loadbalancealgorithmenum,failoversequence=@failoversequence where id=@id", Par);

            if (model.loadbalancealgorithmenum > 0)
            {
                loadbalanceDal.DeleteByConfigId(PubConn, model.id);

                foreach (var item in loadBalanceList)
                {
                    item.configid = model.id;
                    loadbalanceDal.Add(PubConn, item);
                }
            }

            if (model.failoversequence >= 0)
            {
                failoverDal.DeleteByConfigId(PubConn, model.id);
                foreach (var item in failoverList)
                {
                    item.configid = model.id;
                    failoverDal.Add(PubConn, item);
                }
            }
            return true;
        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));

            string Sql = "delete from tb_config where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual bool SetFailoverSequence(DbConn PubConn, int configid, int failoversequence)
        {
           
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //故障转移顺序号：-1 表示关闭故障转移 0 表示当前无故障 大于0表示当前配置故障转移列表中的序号
            Par.Add(new ProcedureParameter("@failoversequence", failoversequence));

            Par.Add(new ProcedureParameter("@id", configid));

            int rev = PubConn.ExecuteSql("update tb_config set failoversequence=@failoversequence where id=@id", Par);

            return rev>0;
        }

        #endregion

        #region Q

        public virtual bool IsExist(DbConn conn, tb_config_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("categoryid", model.categoryid));
            Par.Add(new ProcedureParameter("configkey", model.configkey));
            Par.Add(new ProcedureParameter("id", model.id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select top 1 id from tb_config s where categoryid=@categoryid and configkey=@configkey and id<>@id");
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual tb_config_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_config s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public virtual tb_config_model GetByKey(DbConn PubConn, string configkey)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@configkey", configkey));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_config s where s.configkey=@configkey");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public virtual List<tb_config_model> GetListByConfigids(DbConn PubConn, List<int> configids)
        {
            List<tb_config_model> rs = new List<tb_config_model>();
            if (configids.Count > 0)
            { 
                List<ProcedureParameter> Par = new List<ProcedureParameter>();
                string where = "";
                for (int i = 0; i < configids.Count; i++)
                {
                    Par.Add(new ProcedureParameter("@id" + i, configids[i]));
                    if(i==0)
                        where += string.Format(" id=@id{0} ",i);
                    else
                        where += string.Format(" or id=@id{0} ", i);
                }
                StringBuilder stringSql = new StringBuilder();
                stringSql.Append(@"select s.* from tb_config s where " + where);
                DataSet ds = new DataSet();
                PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach(DataRow dr in ds.Tables[0].Rows)
                        rs.Add( CreateModel(dr));
                }
            }
            return rs;
        }

        public virtual IList<tb_config_model> GetByCategoryId(DbConn PubConn, int id)
        {
            IList<tb_config_model> list = new List<tb_config_model>();
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@categoryid", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_config s where s.categoryid=@categoryid");
            DataTable dt = PubConn.SqlToDataTable(stringSql.ToString(), Par);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    tb_config_model model = CreateModel(dr);
                    list.Add(model);
                }
            }
            return list;
        }

        public IList<tb_config_model> GetPageList(DbConn conn, DateTime? startTime, DateTime? endTime, int categoryid, string configkey, string configvalue,string remark, 
            int pageSize, int pageIndex, ref int count)
        {
            int tempCount = 0;

            IList<tb_config_model> list = new List<tb_config_model>();
            StringBuilder where = new StringBuilder();
            List<ProcedureParameter> parameters = new List<ProcedureParameter>();
            where.Append(" where 1=1");
            //if (startTime != null && endTime != null)
            //{
            //    parameters.Add(new ProcedureParameter("startTime", startTime.Value.ToString("yyyy-MM-dd")));
            //    parameters.Add(new ProcedureParameter("endTime", endTime.Value.ToString("yyyy-MM-dd")));
            //    where.Append(" and s1.createtime>=@starttime and s1.createtime<=@endTime ");
            //}
            if (categoryid > 0)
            {
                parameters.Add(new ProcedureParameter("categoryid", categoryid));
                where.Append(" AND s1.categoryid = @categoryid ");
            }
            if (!string.IsNullOrWhiteSpace(configkey))
            {
                parameters.Add(new ProcedureParameter("configkey", configkey));
                where.Append(" AND s1.configkey like '%'+@configkey+'%' ");
            }
            if (!string.IsNullOrWhiteSpace(configvalue))
            {
                parameters.Add(new ProcedureParameter("configvalue", configvalue));
                where.Append(" AND s1.configvalue like '%'+@configvalue+'%' ");
            }
            if (!string.IsNullOrWhiteSpace(remark))
            {
                parameters.Add(new ProcedureParameter("remark", remark));
                where.Append(" AND s1.remark like '%'+@remark+'%' ");
            }
            StringBuilder sql = new StringBuilder();
            sql.Append("select row_number() over(order by s1.id desc) as rownum,s1.*,s2.name as categoryname from tb_config as s1 with(nolock) left join tb_category as s2 with(nolock) on s1.categoryid = s2.id");
            string countSql = string.Concat("select count(1) from tb_config as s1 with(nolock) ", where.ToString());
            object obj = conn.ExecuteScalar(countSql, parameters);
            if (obj != DBNull.Value && obj != null)
            {
                tempCount = LibConvert.ObjToInt(obj);
            }
            string sqlPage = string.Concat("select * from (", sql.ToString(), where.ToString(), ") a where rownum between ", ((pageIndex - 1) * pageSize + 1), " and ", pageSize * pageIndex);
            DataTable dt = conn.SqlToDataTable(sqlPage, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    tb_config_model model = CreateModel(dr);
                    list.Add(model);
                }
            }
            count = tempCount;
            return list;

        }

        #endregion

        public virtual tb_config_model CreateModel(DataRow dr)
        {
            var o = new tb_config_model();

            //
            if (dr.Table.Columns.Contains("id"))
            {
                o.id = dr["id"].Toint();
            }
            //分类id
            if (dr.Table.Columns.Contains("categoryid"))
            {
                o.categoryid = dr["categoryid"].Toint();
            }
            //配置Key
            if (dr.Table.Columns.Contains("configkey"))
            {
                o.configkey = dr["configkey"].Tostring();
            }
            //配置值
            if (dr.Table.Columns.Contains("configvalue"))
            {
                o.configvalue = dr["configvalue"].Tostring();
            }
            //配置备注
            if (dr.Table.Columns.Contains("remark"))
            {
                o.remark = dr["remark"].Tostring();
            }
            //上一次修改时间
            if (dr.Table.Columns.Contains("lastupdatetime"))
            {
                o.lastupdatetime = dr["lastupdatetime"].ToDateTime();
            }
            //创建时间
            if (dr.Table.Columns.Contains("createtime"))
            {
                o.createtime = dr["createtime"].ToDateTime();
            }
            //负载均衡算法类型: -1 表示 关闭负载均衡，1 表示负载均衡算法
            if (dr.Table.Columns.Contains("loadbalancealgorithmenum"))
            {
                o.loadbalancealgorithmenum = dr["loadbalancealgorithmenum"].Toshort();
            }
            //故障转移顺序号：-1 表示关闭故障转移 0 表示当前无故障 大于0表示当前配置故障转移列表中的序号
            if (dr.Table.Columns.Contains("failoversequence"))
            {
                o.failoversequence = dr["failoversequence"].Toshort();
            }

            if (dr.Table.Columns.Contains("categoryname"))
            {
                o.categoryname = dr["categoryname"].ToString();
            }

            return o;
        }
    }
}