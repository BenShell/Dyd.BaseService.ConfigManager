using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using Dyd.BaseService.ConfigManager.Domain.Model;
using XXF.ProjectTool;

namespace Dyd.BaseService.ConfigManager.Domain.Dal
{
    /*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
    public partial class tb_loadbalance_dal
    {
        public virtual bool Add(DbConn PubConn, tb_loadbalance_model model)
        {

            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //负载均衡配置id
            Par.Add(new ProcedureParameter("@configid", model.configid));
            //负载均衡值
            Par.Add(new ProcedureParameter("@value", model.value));
            //负载均衡权重(1-100之间的int)，负载均衡总值不超过100
            Par.Add(new ProcedureParameter("@boostpercent", model.boostpercent));
            //负载均衡序号
            Par.Add(new ProcedureParameter("@sequence", model.sequence));
            int rev = PubConn.ExecuteSql(@"insert into tb_loadbalance(configid,value,boostpercent,sequence)
                                           values(@configid,@value,@boostpercent,@sequence)", Par);
            return rev == 1;

        }

        public virtual bool Edit(DbConn PubConn, tb_loadbalance_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //负载均衡配置id
            Par.Add(new ProcedureParameter("@configid", model.configid));
            //负载均衡值
            Par.Add(new ProcedureParameter("@value", model.value));
            //负载均衡权重(1-100之间的int)，负载均衡总值不超过100
            Par.Add(new ProcedureParameter("@boostpercent", model.boostpercent));
            //负载均衡序号
            Par.Add(new ProcedureParameter("@sequence", model.sequence));

            Par.Add(new ProcedureParameter("@id", model.id));

            int rev = PubConn.ExecuteSql("update tb_loadbalance set configid=@configid,value=@value,boostpercent=@boostpercent,sequence=@sequence where id=@id", Par);
            return rev == 1;

        }

        public virtual bool SetBoostPercent(DbConn PubConn, int configid, int sequence, int boostpercent)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //负载均衡权重(1-100之间的int)，负载均衡总值不超过100
            Par.Add(new ProcedureParameter("@boostpercent", boostpercent));
            //负载均衡序号
            Par.Add(new ProcedureParameter("@sequence", sequence));

            Par.Add(new ProcedureParameter("@configid", configid));

            int rev = PubConn.ExecuteSql("update tb_loadbalance set boostpercent=@boostpercent where configid=@configid and sequence=@sequence", Par);
            return rev >0;

        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));

            string Sql = "delete from tb_loadbalance where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual bool DeleteByConfigId(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@configid", id));

            string Sql = "delete from tb_loadbalance where configid=@configid";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual tb_loadbalance_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_loadbalance s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public virtual IList<tb_loadbalance_model> GetListByConfigId(DbConn conn, int configId)
        {
            IList<tb_loadbalance_model> list = new List<tb_loadbalance_model>();
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@configid", configId));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_loadbalance s where s.configid=@configid ");
            DataTable dt = conn.SqlToDataTable(stringSql.ToString(), Par);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    tb_loadbalance_model model = CreateModel(dr);
                    list.Add(model);
                }
            }
            return list;
        }

        public virtual List<tb_loadbalance_model> GetListByCategoryIDs(DbConn PubConn, List<int> configids)
        {
            return SqlHelper.Visit((ps) =>
            {
                var rs = new List<tb_loadbalance_model>();
                if (configids != null && configids.Count > 0)
                {
                    StringBuilder stringSql = new StringBuilder();
                    stringSql.AppendFormat(@"select s.* from tb_loadbalance s where s.configid in ({0})", SqlHelper.CmdInFromListForSimplePar<int>(ps, configids, "configid"));
                    DataSet ds = new DataSet();
                    PubConn.SqlToDataSet(ds, stringSql.ToString(), ps.ToParameters());
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            rs.Add(CreateModel(dr));
                        }
                    }
                }
                return rs;
            });
        }

        public virtual tb_loadbalance_model CreateModel(DataRow dr)
        {
            var o = new tb_loadbalance_model();

            //
            if (dr.Table.Columns.Contains("id"))
            {
                o.id = dr["id"].Toint();
            }
            //负载均衡配置id
            if (dr.Table.Columns.Contains("configid"))
            {
                o.configid = dr["configid"].Toint();
            }
            //负载均衡值
            if (dr.Table.Columns.Contains("value"))
            {
                o.value = dr["value"].Tostring();
            }
            //负载均衡权重(1-100之间的int)，负载均衡总值不超过100
            if (dr.Table.Columns.Contains("boostpercent"))
            {
                o.boostpercent = dr["boostpercent"].Toint();
            }
            //负载均衡序号
            if (dr.Table.Columns.Contains("sequence"))
            {
                o.sequence = dr["sequence"].Toshort();
            }
            return o;
        }
    }
}