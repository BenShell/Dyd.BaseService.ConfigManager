using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using Dyd.BaseService.ConfigManager.Domain.Model;

namespace Dyd.BaseService.ConfigManager.Domain.Dal
{
    public partial class tb_systemconfig_dal
    {
        #region C
        public virtual int Add(DbConn PubConn, tb_systemconfig_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            //值
            Par.Add(new ProcedureParameter("@key", model.key));
            //值
            Par.Add(new ProcedureParameter("@value", model.value));
            //备注
            Par.Add(new ProcedureParameter("@remark", model.remark));
            object rev = PubConn.ExecuteSql(@"insert into tb_systemconfig([key],value,remark)
                                           values(@key,@value,@remark);select @@IDENTITY", Par);
            var id = Convert.ToInt32(rev);
            return id;
        }

        public virtual bool Edit(DbConn PubConn, tb_systemconfig_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            Par.Add(new ProcedureParameter("@key", model.key));
            //值
            Par.Add(new ProcedureParameter("@value", model.value));
            //备注
            Par.Add(new ProcedureParameter("@remark", model.remark));

            Par.Add(new ProcedureParameter("@id", model.id));


            int rev = PubConn.ExecuteSql("update tb_systemconfig set [key]=@key, value=@value,remark=@remark where id=@id", Par);
            return rev == 1;

        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));

            string Sql = "delete from tb_systemconfig where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion

        #region Q

        public virtual bool IsExist(DbConn conn, tb_systemconfig_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("key", model.key));
            Par.Add(new ProcedureParameter("id", model.id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select top 1 id from tb_systemconfig s where [key]=@key and id<>@id");
            DataSet ds = new DataSet();
            conn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual tb_systemconfig_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_systemconfig s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }   
        
        public virtual tb_systemconfig_model GetByKey(DbConn PubConn, string key)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@key", key));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_systemconfig s where s.[key]=@key");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public IList<tb_systemconfig_model> GetPageList(DbConn conn, string key, string value, string remark, int pageIndex, int pageSize, ref int count)
        {
            int tempCount = 0;

            IList<tb_systemconfig_model> list = new List<tb_systemconfig_model>();
            StringBuilder where = new StringBuilder();
            List<ProcedureParameter> parameters = new List<ProcedureParameter>();
            where.Append(" where 1=1");
            if (!string.IsNullOrWhiteSpace(key))
            {
                parameters.Add(new ProcedureParameter("key", key));
                where.Append(" and [key] like '%'+@key+'%' ");
            }
            if (!string.IsNullOrWhiteSpace(value))
            {
                parameters.Add(new ProcedureParameter("value", value));
                where.Append(" and value like '%'+@value+'%' ");
            }
            if (!string.IsNullOrWhiteSpace(remark))
            {
                parameters.Add(new ProcedureParameter("remark", remark));
                where.Append(" and remark like '%'+@remark+'%' ");
            }
            StringBuilder sql = new StringBuilder();
            sql.Append("select row_number() over(order by id desc) as rownum,* from tb_systemconfig with(nolock)");
            string countSql = string.Concat("select count(1) from tb_systemconfig with(nolock) ", where.ToString());
            object obj = conn.ExecuteScalar(countSql, parameters);
            if (obj != DBNull.Value && obj != null)
            {
                tempCount = LibConvert.ObjToInt(obj);
            }
            string sqlPage = string.Concat("select * from (", sql.ToString(), where.ToString(), ") a where rownum between ", ((pageIndex - 1) * pageSize + 1), " and ", pageSize * pageIndex);
            DataTable dt = conn.SqlToDataTable(sqlPage, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    tb_systemconfig_model model = CreateModel(dr);
                    list.Add(model);
                }
            }
            count = tempCount;
            return list;
        }
        #endregion

        public virtual tb_systemconfig_model CreateModel(DataRow dr)
        {
            var o = new tb_systemconfig_model();

            if (dr.Table.Columns.Contains("id"))
            {
                o.id = dr["id"].Toint();
            }
            //key
            if (dr.Table.Columns.Contains("key"))
            {
                o.key = dr["key"].Tostring();
            }
            //值
            if (dr.Table.Columns.Contains("value"))
            {
                o.value = dr["value"].Tostring();
            }
            //备注
            if (dr.Table.Columns.Contains("remark"))
            {
                o.remark = dr["remark"].Tostring();
            }
            return o;
        }
    }
}