using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using Dyd.BaseService.ConfigManager.Domain.Model;
using XXF.ProjectTool;

namespace Dyd.BaseService.ConfigManager.Domain.Dal
{
    /*�����Զ����ɹ����Զ�����,��Ҫ������д�Լ��Ĵ��룬����ᱻ�Զ�����Ŷ - ����*/
    public partial class tb_failover_dal
    {
        public virtual bool Add(DbConn PubConn, tb_failover_model model)
        {

            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //����ת������id
            Par.Add(new ProcedureParameter("@configid", model.configid));
            //����ת��ֵ
            Par.Add(new ProcedureParameter("@value", model.value));
            //����ת��˳���
            Par.Add(new ProcedureParameter("@sequence", model.sequence));
            int rev = PubConn.ExecuteSql(@"insert into tb_failover(configid,value,sequence)
                                           values(@configid,@value,@sequence)", Par);
            return rev == 1;

        }

        public virtual bool Edit(DbConn PubConn, tb_failover_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();

            //����ת������id
            Par.Add(new ProcedureParameter("@configid", model.configid));
            //����ת��ֵ
            Par.Add(new ProcedureParameter("@value", model.value));
            //����ת��˳���
            Par.Add(new ProcedureParameter("@sequence", model.sequence));

            Par.Add(new ProcedureParameter("@id", model.id));

            int rev = PubConn.ExecuteSql("update tb_failover set configid=@configid,value=@value,sequence=@sequence where id=@id", Par);
            return rev == 1;

        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));

            string Sql = "delete from tb_failover where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual bool DeleteByConfigId(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@configid", id));
            string Sql = "delete from tb_failover where configid=@configid";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual tb_failover_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_failover s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }


        public virtual IList<tb_failover_model> GetList(DbConn conn)
        {
            IList<tb_failover_model> list = new List<tb_failover_model>();
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_failover s");
            DataTable dt = conn.SqlToDataTable(stringSql.ToString(), Par);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    tb_failover_model model = CreateModel(dr);
                    list.Add(model);
                }
            }
            return list;
        }

        public virtual IList<tb_failover_model> GetListByConfigId(DbConn conn, int configId)
        {
            IList<tb_failover_model> list = new List<tb_failover_model>();
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@configid", configId));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_failover  s where s.configid=@configid ");
            DataTable dt = conn.SqlToDataTable(stringSql.ToString(), Par);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    tb_failover_model model = CreateModel(dr);
                    list.Add(model);
                }
            }
            return list;
        }

        public virtual List<tb_failover_model> GetListByCategoryIDs(DbConn PubConn, List<int> configids)
        {
            return SqlHelper.Visit((ps) =>
            {
                var rs = new List<tb_failover_model>();
                if (configids != null && configids.Count > 0)
                {
                    StringBuilder stringSql = new StringBuilder();
                    stringSql.AppendFormat(@"select s.* from tb_failover s where s.configid in ({0})", SqlHelper.CmdInFromListForSimplePar<int>(ps, configids, "configid"));
                    DataSet ds = new DataSet();
                    PubConn.SqlToDataSet(ds, stringSql.ToString(), ps.ToParameters());
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            rs.Add(CreateModel(dr));
                        }
                    }
                }
                return rs;
            });
        }

        public virtual tb_failover_model CreateModel(DataRow dr)
        {
            var o = new tb_failover_model();

            //
            if (dr.Table.Columns.Contains("id"))
            {
                o.id = dr["id"].Toint();
            }
            //����ת������id
            if (dr.Table.Columns.Contains("configid"))
            {
                o.configid = dr["configid"].Toint();
            }
            //����ת��ֵ
            if (dr.Table.Columns.Contains("value"))
            {
                o.value = dr["value"].Tostring();
            }
            //����ת��˳���
            if (dr.Table.Columns.Contains("sequence"))
            {
                o.sequence = dr["sequence"].Toshort();
            }
            return o;
        }
    }
}