using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dyd.BaseService.ConfigManager.Domain.Model
{
    /// <summary>
    /// tb_category Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_category_model
    {

        public tb_category_model()
        {
            this.tb_config_model = new List<tb_config_model>();
        }
        [Display]
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 分类名
        /// </summary>
        [Display(Name = "分类名")]
        [StringLength(200)]
        [Required]
        public string name { get; set; }

        /// <summary>
        /// 分类备注
        /// </summary>
        [Display(Name = "分类备注")]
        [Required]
        [StringLength(2000)]
        public string remark { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime createtime { get; set; }


        public virtual IList<tb_config_model> tb_config_model { get; set; }

    }
}