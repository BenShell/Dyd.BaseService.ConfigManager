﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dyd.BaseService.ConfigManager.Domain.Model
{
    /// <summary>
    /// 操作结果
    /// </summary>
    public class OperateResults
    {
        public bool Flag { get; set; }
        public string Message { get; set; }
    }
}
