using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dyd.BaseService.ConfigManager.Domain.Model
{
    /// <summary>
    /// tb_project Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_project_model
    {
        public tb_project_model()
        {
            this.tb_category_model = new List<tb_category_model>();
        }

        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        [Display(Name = "项目名称")]
        [Required]
        [StringLength(100)]
        public string projectname { get; set; }

        /// <summary>
        /// 项目备注
        /// </summary>
        [Display(Name = "备注")]
        [Required]
        [StringLength(5000)]
        public string remark { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime createtime { get; set; }

        /// <summary>
        /// 相关分类id，号分隔，号结尾
        /// </summary>
        [Display(Name = "配置分类")]
        public string categoryids { get; set; }

        /// <summary>
        /// 项目最后心跳时间
        /// </summary>
        [Display(Name = "项目最后心跳时间")]
        public DateTime lastheartbeattime { get; set; }

        public virtual IList<tb_category_model> tb_category_model { get; set; }

        /// <summary>
        /// 分类
        /// </summary>
        [Display(Name = "分类")]
        public string categorynames { get; set; }

    }
}