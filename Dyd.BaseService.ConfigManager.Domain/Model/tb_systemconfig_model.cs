using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dyd.BaseService.ConfigManager.Domain.Model
{
    /// <summary>
    /// tb_systemconfig Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_systemconfig_model
    {
        /// <summary>
        /// id
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// key
        /// </summary>
        [Display(Name = "Key")]
        public string key { get; set; }

        /// <summary>
        /// ֵ
        /// </summary>
        [Display(Name = "ֵ")]
        public string value { get; set; }

        /// <summary>
        /// ��ע
        /// </summary>
        [Display(Name = "��ע")]
        public string remark { get; set; }

    }
}