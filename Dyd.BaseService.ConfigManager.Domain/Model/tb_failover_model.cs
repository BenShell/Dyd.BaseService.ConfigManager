using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Dyd.BaseService.ConfigManager.Domain.Model
{
    /// <summary>
    /// tb_failover Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_failover_model
    {
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// ����ת������id
        /// </summary>
        public int configid { get; set; }

        /// <summary>
        /// ����ת��ֵ
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// ����ת��˳���
        /// </summary>
        public short sequence { get; set; }

    }
}