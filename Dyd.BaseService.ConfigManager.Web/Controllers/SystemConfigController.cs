﻿using Dyd.BaseService.ConfigManager.Domain;
using Dyd.BaseService.ConfigManager.Domain.Dal;
using Dyd.BaseService.ConfigManager.Domain.Model;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;
using XXF.Db;
using Dyd.BaseService.ConfigManager.Web.Tools;

namespace Dyd.BaseService.ConfigManager.Web.Controllers
{
    [Authorize]
    public class SystemConfigController : Controller
    {
        private tb_log_dal logDal = new tb_log_dal();
        private tb_systemconfig_dal systemconfigDal = new tb_systemconfig_dal();

        #region 列表
        public ActionResult SystemConfigIndex(string key, string value, string remark, int pageIndex = 1, int pageSize = 20)
        {
            int count = 0;
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ConfigMangeConnectString))
            {
                ViewBag.Key = key;
                ViewBag.Value = value;
                ViewBag.Remark = remark;
                conn.Open();
                IList<tb_systemconfig_model> list = systemconfigDal.GetPageList(conn, key, value, remark, pageIndex, pageSize, ref count);
                PagedList<tb_systemconfig_model> pageList = new PagedList<tb_systemconfig_model>(list, pageIndex, pageSize, count);
                if (Request.IsAjaxRequest())
                {
                    return PartialView("_SystemConfigIndex", pageList);
                }
                else
                {
                    return View(pageList);
                }
            }
        }
        #endregion

        #region 新增
        public ActionResult SystemConfigAdd()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SystemConfigAdd(tb_systemconfig_model model)
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ConfigMangeConnectString))
            {
                conn.Open();
                try
                {
                    if (systemconfigDal.IsExist(conn, model))
                    {
                        return Json(new OperateResults { Flag = false, Message = "Key重复！" }, JsonRequestBehavior.AllowGet);
                    }
                    var id = systemconfigDal.Add(conn, model);
                    logDal.Add(conn, string.Format("{0}|Id:{1}|Key:{2}|Value:{3}", "添加系统配置", id, model.key, model.value));
                    RedisHelper.LoadRedisServer();
                    return Json(new OperateResults { Flag = true, Message = "添加成功！" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception exp)
                {
                    return Json(new OperateResults { Flag = false, Message = exp.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region 修改
        public ActionResult SystemConfigEdit(int id)
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ConfigMangeConnectString))
            {
                conn.Open();
                var model = systemconfigDal.Get(conn, id);
                return View(model);
            };
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SystemConfigEdit(tb_systemconfig_model model)
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ConfigMangeConnectString))
            {
                conn.Open();
                try
                {
                    if (systemconfigDal.IsExist(conn, model))
                    {
                        return Json(new OperateResults { Flag = false, Message = "Key重复！" }, JsonRequestBehavior.AllowGet);
                    }
                    systemconfigDal.Edit(conn, model);
                    logDal.Add(conn, string.Format("{0}|Id:{1}|Key:{2}|Value:{3}", "修改系统配置", model.id, model.key, model.value));
                    RedisHelper.LoadRedisServer();
                    return Json(new OperateResults { Flag = true, Message = "修改成功！" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception exp)
                {
                    return Json(new OperateResults { Flag = false, Message = exp.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region 删除
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SystemConfigDel(int id)
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ConfigMangeConnectString))
            {
                try
                {
                    conn.Open();
                    var model = systemconfigDal.Get(conn, id);
                    systemconfigDal.Delete(conn, id);
                    logDal.Add(conn, string.Format("{0}|Id:{1}|Key:{2}|Value:{3}", "删除系统配置", model.id, model.key, model.value));
                    return Json(new OperateResults { Flag = true, Message = "删除成功！" });
                }
                catch (Exception exp)
                {
                    return Json(new OperateResults { Flag = false, Message = exp.Message });
                }
            }
        }
        #endregion
    }
}
