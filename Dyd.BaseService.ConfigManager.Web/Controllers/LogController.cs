﻿using Dyd.BaseService.ConfigManager.Domain;
using Dyd.BaseService.ConfigManager.Domain.Dal;
using Dyd.BaseService.ConfigManager.Domain.Model;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;
using XXF.Db;

namespace Dyd.BaseService.ConfigManager.Web.Controllers
{
    [Authorize]
    public class LogController : Controller
    {
        private tb_log_dal logDal = new tb_log_dal();
        #region 列表
        public ActionResult LogIndex(DateTime? startTime, DateTime? endTime, string projectname, string info, int pageIndex = 1, int pageSize = 20)
        {

            if (startTime != null)
            {
                startTime = startTime.Value;
            }
            else
            {
                startTime = DateTime.Now.AddDays(-30);
            }
            if (endTime != null)
            {
                endTime = endTime.Value;
            }
            else
            {
                endTime = DateTime.Now.AddDays(1);
            }

            ViewBag.startTime = startTime;
            ViewBag.endTime = endTime;
            ViewBag.projectname = projectname;
            ViewBag.info = info;

            int count = 0;
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ConfigMangeConnectString))
            {
                conn.Open();
                IList<tb_log_model> list = logDal.GetPageList(conn, startTime, endTime, projectname, info, pageSize, pageIndex, ref count);
                PagedList<tb_log_model> pageList = new PagedList<tb_log_model>(list, pageIndex, pageSize, count);
                if (Request.IsAjaxRequest())
                {
                    return PartialView("_LogIndex", pageList);
                }
                else
                {
                    return View(pageList);
                }
            }
        }
        #endregion

        #region 清理日志
        [HttpPost]
        public JsonResult LogClear()
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ConfigMangeConnectString))
            {
                conn.Open();
                logDal.Clear(conn);
                logDal.Add(conn, string.Format("{0}", "清理日志"));
                return Json(new OperateResults { Flag = true, Message = "清理成功！" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}
