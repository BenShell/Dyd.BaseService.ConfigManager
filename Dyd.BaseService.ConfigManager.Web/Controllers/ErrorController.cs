﻿using Dyd.BaseService.ConfigManager.Domain;
using Dyd.BaseService.ConfigManager.Domain.Dal;
using Dyd.BaseService.ConfigManager.Domain.Model;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;
using XXF.Db;

namespace Dyd.BaseService.ConfigManager.Web.Controllers
{
    [Authorize]
    public class ErrorController : Controller
    {
        private tb_log_dal logDal = new tb_log_dal();
        private tb_error_dal errorDal = new tb_error_dal();
        #region 列表
        public ActionResult ErrorIndex(DateTime? startTime, DateTime? endTime, string projectname, string info, int pageIndex = 1, int pageSize = 20)
        {

            if (startTime != null)
            {
                startTime = startTime.Value;
            }
            else
            {
                startTime = DateTime.Now.AddDays(-30);
            }
            if (endTime != null)
            {
                endTime = endTime.Value;
            }
            else
            {
                endTime = DateTime.Now.AddDays(1);
            }

            ViewBag.startTime = startTime;
            ViewBag.endTime = endTime;
            ViewBag.projectname = projectname;
            ViewBag.info = info;
            int count = 0;
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ConfigMangeConnectString))
            {
                conn.Open();
                IList<tb_error_model> list = errorDal.GetPageList(conn, startTime, endTime, projectname, info, pageSize, pageIndex, ref count);
                PagedList<tb_error_model> pageList = new PagedList<tb_error_model>(list, pageIndex, pageSize, count);
                if (Request.IsAjaxRequest())
                {
                    return PartialView("_ErrorIndex", pageList);
                }
                else
                {
                    return View(pageList);
                }
            }
        }
        #endregion

        #region 情况日志
        public JsonResult ErrorClear()
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ConfigMangeConnectString))
            {
                conn.Open();
                errorDal.Clear(conn);
                logDal.Add(conn, string.Format("{0}", "清空错误"));
                return Json(new OperateResults { Flag = true, Message = "清理成功！" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}
