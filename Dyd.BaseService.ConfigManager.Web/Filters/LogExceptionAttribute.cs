﻿using Dyd.BaseService.ConfigManager.Domain;
using Dyd.BaseService.ConfigManager.Domain.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XXF.Db;

namespace Dyd.BaseService.ConfigManager.Web.Filters
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class LogExceptionAttribute : HandleErrorAttribute
    {
        private tb_error_dal errorDal = new tb_error_dal();
        public override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                string controllerName = (string)filterContext.RouteData.Values["controller"];
                string actionName = (string)filterContext.RouteData.Values["action"];
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ConfigMangeConnectString))
                {
                    conn.Open();
                    try
                    {
                        errorDal.Add(conn, string.Format("Controller:{0}|Action:{1}|Message:{2}", controllerName, actionName, filterContext.Exception.Message), "配置中心管理网站");
                    }
                    catch
                    {
                    }
                }
                base.OnException(filterContext);
            }
        }
    }
}