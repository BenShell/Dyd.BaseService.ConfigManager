﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XXF.BaseService.ConfigManager.SystemRuntime;

namespace Dyd.BaseService.ConfigManager.Web.Tools
{

    public class RedisNetCommand
    {
        private string redisServerIp;

        public RedisNetCommand(string rediserverip)
        {
            redisServerIp = rediserverip;
        }
        /// <summary>
        /// 可丢消息发送,当前消息并发情况下丢弃
        /// </summary>
        /// <param name="mqpath"></param>
        public void SendMessage(XXF.BaseService.ConfigManager.SystemRuntime.ConfigNetCommand command)
        {
            var manager = new XXF.Redis.RedisManager();
            using (var c = manager.CreateClient(redisServerIp.Split(':')[0], Convert.ToInt32(redisServerIp.Split(':')[1]), "").GetClient())
            {
                try
                {
                     c.PublishMessage(XXF.BaseService.ConfigManager.SystemRuntime.SystemParamConfig.Redis_Channel, new XXF.Serialization.JsonHelper().Serializer(command));
                     c.Quit();
                }
                catch (Exception exp)
                {
                    try { c.Quit(); }
                    catch { }
                    throw exp;
                }
            }
        }


    }
}