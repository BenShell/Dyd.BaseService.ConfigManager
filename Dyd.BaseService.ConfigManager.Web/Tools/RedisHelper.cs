﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dyd.BaseService.ConfigManager.Domain;
using XXF.Db;

namespace Dyd.BaseService.ConfigManager.Web.Tools
{
    public class RedisHelper
    {
        public static string RedisServer { get; set; }
        public static void SendCommand(XXF.BaseService.ConfigManager.SystemRuntime.ConfigNetCommand command)
        {
            if (string.IsNullOrWhiteSpace(RedisServer))
                LoadRedisServer();
            new RedisNetCommand(RedisServer).SendMessage(command);
        }

        public static void LoadRedisServer()
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ConfigMangeConnectString))
            {
                conn.Open();
                var config = new Domain.Dal.tb_systemconfig_dal().GetByKey(conn,XXF.BaseService.ConfigManager.SystemRuntime.EnumSystemConfigKey.RedisServer.ToString());
                if (config != null)
                    RedisServer = config.value;
            }
        }
    }
}