﻿using Dyd.BaseService.ConfigManager.Web.Filters;
using System.Web;
using System.Web.Mvc;

namespace Dyd.BaseService.ConfigManager.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new LogExceptionAttribute());
        }
    }
}